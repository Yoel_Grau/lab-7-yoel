public class Circle implements Form {

    public double radio;

    public Circle(double radio){
        this.radio = radio;
    }

    @Override
    public double calculateArea() {
        return 4 * Math.PI * Math.pow(radio, 2);
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * radio;
    }

    @Override
    public String toString() {
        return "Circle: " +
                " Radio = " + this.radio;
    }
}
