public interface Form {
    double calculateArea();
    double calculatePerimeter();
}
