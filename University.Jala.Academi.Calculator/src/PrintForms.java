public class PrintForms {
    public static void printForms(Form form){
        System.out.println(form.toString());
        System.out.println("Area: " + form.calculateArea());
        System.out.println("Perimeter: " + form.calculatePerimeter());
    }
}
