import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("** Calculate area and perimiter of shapes **");

        Form rectangle = new Rectangle(8, 2);
        Form circle = new Circle(5);
        Form triangle = new Triangle(4, 7);

        for (Form form : Arrays.asList(rectangle, circle, triangle)) {
            PrintForms.printForms(form);
        }
    }
}
