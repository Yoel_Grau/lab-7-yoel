import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    @Test
    public void test1() {
        Circle circle = new Circle(5);
        assertEquals(4 * Math.PI * 25, circle.calculateArea());
    }

    @Test
    public void test2() {
        Circle circle = new Circle(5);
        assertEquals(2 * Math.PI * 5, circle.calculatePerimeter());
    }

    @Test
    public void test3() {
        Circle circle = new Circle(5);
        assertEquals("Circle:  Radio = 5.0", circle.toString());
    }

    @Test
    public void test4() {
        Rectangle rectangle = new Rectangle(8, 2);
        assertEquals(16, rectangle.calculateArea());
    }

    @Test
    public void test5() {
        Rectangle rectangle = new Rectangle(8, 2);
        assertEquals(20, rectangle.calculatePerimeter());
    }

    @Test
    public void test6() {
        Rectangle rectangle = new Rectangle(8, 2);
        assertEquals("Rectable:  Width = 2.0 Height = 8.0", rectangle.toString());
    }

    @Test
    public void test8() {
        Triangle triangle = new Triangle(4, 7);
        assertEquals(2 * (4 + 7), triangle.calculatePerimeter());
    }

    @Test
    public void test9() {
        Triangle triangle = new Triangle(4, 7);
        assertEquals("Triangle:  Height = 4.0 Base = 7.0", triangle.toString());
    }
}
